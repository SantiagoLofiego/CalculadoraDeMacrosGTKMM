#include <string>
#include "CalculadoraDeMacros.hpp"

namespace CalculadoraDeMacros
{
	namespace LogicaNegocio
	{
		CalculadoraDeMacros::CalculadoraDeMacros(){};
		CalculadoraDeMacros::~CalculadoraDeMacros(){};

		float CalculadoraDeMacros::MetabolismoBasal(SEXO sexo, float altura, int idade, float peso)
		{
			if(SEXO::HOMEM == sexo)
			{
				return 66.f+13.7f*peso + 5.f*altura*100.f - 6.8f*static_cast<float>(idade);
			}
			else if (SEXO::MULHER == sexo)
			{
				return 655.f+9.6f*peso+1.8f*altura*100.f - 4.7f*static_cast<float>(idade);
			}
			else
			{
				throw new std::string("[ERRO]LogicaNegocio::CalculadoraDeMacros::MetabolismoBasal recebeu sexo inválido");
			}
		}

		float CalculadoraDeMacros::EstimativaCalorica(float metabolismoBasal, NIVEL_ATIVIDADE nvAtividade)
		{
			if(NIVEL_ATIVIDADE::SEDENTARIO == nvAtividade)
			{
				return metabolismoBasal*1.2f;
			}
			else if(NIVEL_ATIVIDADE::LEVEMENTE_ATIVO == nvAtividade)
			{
				return metabolismoBasal*1.375f;
			}
			else if(NIVEL_ATIVIDADE::MODERADAMENTE_ATIVO == nvAtividade)
			{
				return metabolismoBasal*1.55f;
			}
			else if(NIVEL_ATIVIDADE::ALTAMENTE_ATIVO == nvAtividade)
			{
				return metabolismoBasal*1.725f;
			}
			else if(NIVEL_ATIVIDADE::EXTREMAMENTE_ATIVO == nvAtividade)
			{
				return metabolismoBasal*1.9f;
			}
			else
			{
				throw new std::string("[ERRO]LogicaNegocio::CalculadoraDeMacros::EstimativaCalorica recebeu nível de atividade inválido");
			}
		}

		float CalculadoraDeMacros::CaloriasPlanejadas(float carbo, float prote, float gord)
		{
			return carbo*4+prote*4+gord*9;
		}

		float CalculadoraDeMacros::CaloriasPlanejadas(float peso, float carboPorKg, float protePorKg, float gordPorKg)
		{
			return CaloriasPlanejadas(carboPorKg*peso, protePorKg*peso, gordPorKg*peso);
		}

		float CalculadoraDeMacros::CarbPorKgPraCarbTotal(float peso, float carbPorKg)
		{
			return peso*carbPorKg;
		}
		float CalculadoraDeMacros::CarbTotalPraCarbPorKg(float peso, float carbTotal)
		{
			return carbTotal/peso;
		}

		float CalculadoraDeMacros::ProPorKgPraProTotal(float peso, float proPorKg)
		{
			return peso*proPorKg;
		}
		float CalculadoraDeMacros::PrototalPraProPorKg(float peso, float proTotal)
		{
			return proTotal/peso;
		}

		float CalculadoraDeMacros::GorPorKgPraGorTotal(float peso, float gorPorKg)
		{
			return peso*gorPorKg;
		}
		float CalculadoraDeMacros::GortotalPraGorPorKg(float peso, float gorTotal)
		{
			return gorTotal/peso;
		}
	}
}
