#ifndef CALCULADORADEMACROS_HPP
#define CALCULADORADEMACROS_HPP

#include "TiposBasicos.hpp"

namespace CalculadoraDeMacros
{
	namespace LogicaNegocio
	{
		class CalculadoraDeMacros
		{
			public:
				CalculadoraDeMacros();
				~CalculadoraDeMacros();

				float MetabolismoBasal(SEXO sexo, float altura, int idade, float peso);
				float EstimativaCalorica(float metabolismoBasal, NIVEL_ATIVIDADE nvAtividade);

				float CaloriasPlanejadas(float peso, float carboPorKg, float protePorKg, float gordPorKg);
				float CaloriasPlanejadas(float carbo, float prote, float gord);

				float CarbPorKgPraCarbTotal(float peso, float carbPorKg);
				float CarbTotalPraCarbPorKg(float peso, float carbTotal);

				float ProPorKgPraProTotal(float peso, float proPorKg);
				float PrototalPraProPorKg(float peso, float proTotal);

				float GorPorKgPraGorTotal(float peso, float gorPorKg);
				float GortotalPraGorPorKg(float peso, float gorTotal);
		};
	}
}
#endif // CALCULADORADEMACROS_HPP
