#include <gtkmm-3.0/gtkmm.h>

int main(int argc, char** argv)
{
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create (argc, argv, "org.anders1232.CalculadoraDeMacros.Gtk");
	Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("/home/francisco/Git/GtkCalcDeMacros/JanelaPrincipal.glade");
	Glib::RefPtr<Gtk::ApplicationWindow> janelaPrincipal= Glib::RefPtr<Gtk::ApplicationWindow>::cast_dynamic(builder->get_object("JanelaPrincipal"));
//	janelaPrincipal.set_title("Calculadora de Macros by Anders1232");
	app->run( *(janelaPrincipal.get()), argc, argv);
	return 0;
}
