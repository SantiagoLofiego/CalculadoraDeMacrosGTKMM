TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -finline-functions -Wall -pedantic `pkg-config gtkmm-3.0 --cflags --libs`
QMAKE_CFLAGS += -finline-functions -Wall -pedantic `pkg-config gtkmm-3.0 --cflags --libs`

QMAKE_LFLAGS += `pkg-config gtkmm-3.0 --cflags --libs`
QMAKE_LINK += `pkg-config gtkmm-3.0 --cflags --libs`

QMAKE_CFLAGS_DEBUG += -O0
QMAKE_CXXFLAGS_DEBUG += -O0

QMAKE_CFLAGS_RELEASE += -O3
QMAKE_CXXFLAGS_RELEASE += -O3

LIBS += `pkg-config gtkmm-3.0 --libs`
QMAKE

SOURCES += \
        CalculadoraDeMacros.cpp \
        apresentacao.cpp \
        main.cpp

HEADERS += \
	CalculadoraDeMacros.hpp \
	TiposBasicos.hpp \
	apresentacao.hpp
